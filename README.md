# Verifiable Secret Sharing for Shamir Threshold Scheme

This implementation was part of the contribution for the following paper:

Bailey Kacsmar, Chelsea Komlo, Florian Kerschbaum, Ian Goldberg.
"Mind the Gap: Ceremonies for Applied Secret Sharing."
Proceedings on Privacy Enhancing Technologies. Vol. 2020, No. 2. 18
pages. April 2020.

This library implements a subset of the improvements to Shamir's
threshold scheme presented in this paper. As such, this library provides the
ability for participants to verify the integrity of shares received by a central dealer,
and for participants to update shares in the future, providing protection
against an adversary who can collect a threshold number of shares across
update epochs, but not within a single epoch.

## Use

You will need rust and cargo installed to use this library.  The library
was successfully tested with rustc 1.38.0.  See the tests in
[vss.rs](src/vss.rs#L167) for examples of how to use the library.

Do not use this code in production.

Note that this library does not provide support for serialization and
deserialization of shares in order to securely transmit and store shares after
they have been generated. Furthermore, the caller of ```generate_shares``` will
need to account for securely deleting shares once they have been properly
transmitted to participants.

Furthermore, this library does not account for how participants should handle
the case when a share or update is not valid.

## Additional Improvements

Note that although the paper discusses additional improvements to Shamir's
Threshold Scheme, we did not include all improvements in this library due the
variance in implementation requirements. For example, when operating in
Extended Mode, the integrity value can be distributed along with shares in
order to allow for validating the secret document upon recovery. However, as
platform and security requirements can change how documents are encrypted, we
opted to not include improvements for the Extended mode of operation in this
library; see the paper (Figure 5 specifically) for more details.

## Development

Development on this project is frozen and will not implement any additional features.
Forking this project to extend features is welcome.

Running tests for this project is standard to any Cargo library. To run tests,
run:

```
cargo test
```

from the top-level directory of the repository.
